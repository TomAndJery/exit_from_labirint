#include <stdio.h>
#include <stdlib.h>

typedef struct{
    int x;
    int y;
}mapPoint;

typedef struct targetListPart{
   struct targetListPart *nextPart;
   int x;
   int y;
}targetListPart;

typedef struct {
    targetListPart *first;
    targetListPart *last;
    int count;
}targetList;

int *arr;
int *arr2;
int arrSizeY=0;
int arrSizeX=0;
int startY=0;
int startX=0;
int finishY=0;
int finishX=0;

targetList arrOut;



int initTargetList( targetList *adr){
    adr->count=0;
    adr->first=NULL;
    adr->last=NULL;
    return 0;
}


int pushTargetListPart(targetList *list,int y,int x){
    if(list==NULL){
        printf("pushTargetListPart: ERROR list pointer == NULL\n");
        return 1;
    }
    targetListPart *temp=(targetListPart*)calloc(1,sizeof(targetListPart));
    temp->x =x;
    temp->y =y;
    temp->nextPart =NULL;
    if (list->last!=NULL) list->last->nextPart=temp;
    list->last=temp;
    if (list->first==NULL) list->first=list->last;
    list->count++;
    return 0;
}

int popTArgetList(targetList* list,int*y,int *x){
    if (list==NULL){
        printf("popVoidList: ERROR list pointer == NULL\n");
        return 1;
    }
    if (list->first==NULL){
        printf("popVoidList: list empy return == NULL\n");
        return 1;
    }
    targetListPart *temp =list->first;
    if(list->first==list->last){
        list->first=NULL;
        list->last=NULL;
    }else  list->first= list->first->nextPart;
    *x=temp->x;
    *y=temp->y;
    free(temp);
    list->count--;
    return 0;
}

void clearTargetList(targetList* list){
    int tempX,tempY;
    while(list->count!=0){
        popTArgetList(list,&tempX,&tempY);
    }
}

void printMapCombi(){
    int x,y;
    int adr;
    for (y=0;y<arrSizeY;y++){
       // printf("");
        for(x=0;x<arrSizeX;x++){
            adr = arrSizeX*y+x;

            if(*(arr+adr)==' '){
                if(*(arr2+adr)!=0) printf(".");
                else printf(" ");
            }else printf("%c",*(arr+adr));
        }
        printf("\n");
    }
}

int findSizeMap(char *name){
    int txmax=0,tx=0,ty=0;
    FILE *f;
    f=fopen(name,"r");
    if(f==NULL) {
        printf("not find %s\n",name);
        return 1;
    }
    ty=1;
    char r=fgetc(f);
    while(r!=EOF){
        if (r==10){
            if(tx>txmax) txmax=tx;
            ty++;
            tx=0;
        } else tx++;
        r=fgetc(f);
    }
    if(tx>txmax) txmax=tx;
    fclose(f);
    if (txmax==0) return 1;
    arrSizeY=ty+2;
    arrSizeX=txmax+2;
    return 0;
}


int  loadMap2(char *name){
    if (findSizeMap(name)!=0) return 1;
    FILE *f;
    int x,y;
    char *buffer=(char*)calloc(arrSizeX,sizeof(char));
    f=fopen(name,"r");
    arr=(int*)calloc(arrSizeX*arrSizeY,sizeof(int));

    initTargetList(&arrOut);

    for(y=0;y<arrSizeY;y++)
        for(x=0;x<arrSizeX;x++){
            *(arr+arrSizeX*y+x)='*';
        }
    for(y=1;y<arrSizeY-1;y++){
        if (fgets(buffer,arrSizeX,f)==NULL) *buffer=0;
        for(x=1;x<arrSizeX;x++){
            if(*(buffer+x-1)=='\n') x=arrSizeX;
            else if(*(buffer+x-1)==0) x=arrSizeX;
            else {
                *(arr+arrSizeX*y+x)=*(buffer+x-1);
                if (*(buffer+x-1)== 'F'){
                   printf("load Finish point n#%i y=%i x=%i\n",arrOut.count,y,x);
                   pushTargetListPart(&arrOut,y,x);
                   finishX=x;
                   finishY=y;
                   *(arr+arrSizeX*y+x)=' ';
                }else if (*(buffer+x-1)== 'S'){
                   startX=x;
                   startY=y;
                   *(arr+arrSizeX*y+x)=' ';
                }

            }

        }
    }
    printf("start  position  y=%i x=%i\n",startY,startX);
    free(buffer);
    printf("count of OUT = %i\n",arrOut.count);
    return 0;
}

void findOnePath(){
    int adr,x,y,n;
    int *arrTemp=(int*)calloc(arrSizeX*arrSizeY,sizeof(int));
    x=finishX;
    y=finishY;
    adr= arrSizeX*y+x;
    n=*(arr2+adr);
    *(arrTemp+arrSizeX*y+x)=n;

    while(n!=1){
        if(n-1==*(arr2+(arrSizeX*(y+1)+x))){//y+1
            y++;
            n--;
        }else if(n-1==*(arr2+(arrSizeX*(y-1)+x))){//y-1
            y--;
            n--;
        }else if(n-1==*(arr2+(arrSizeX*y+x+1))){//x+1
            x++;
            n--;
        }else if(n-1==*(arr2+(arrSizeX*y+x-1))){//y+1
            x--;
            n--;
        }
        *(arrTemp+arrSizeX*y+x)=n;
    }
    int * tmp;
    tmp=arr2;
    arr2=arrTemp;
    free(tmp);
}

void core(){
    char filename[50];
    printf("Enter file name (file with map)-->");
    scanf("%s",filename);
    getchar();
    printf(">%s<\n",filename);
    if(loadMap2(filename)!=0){// load from txt
        return;
    }
    if ((startX==0)||(startY==0)||(finishX==0)||(finishY==0)){
        printf("In map not finded 'S'-Stars  or 'F'-Finish\n");
        printf("Edit your map!!!\n");
        getchar();
    }

    arr2=(int*)calloc(arrSizeX*arrSizeY,sizeof(int));//create arr of steps
    int adr=arrSizeX*startY+startX;
    *(arr2+adr)=1;//push step nom 1 in map of steps

    targetList  listTarget;
    initTargetList(&listTarget);// init list of branch
    pushTargetListPart(&listTarget,startY,startX);//push in list first branch (start y x)
    int x,y,n;
    int nOut;
    while(popTArgetList(&listTarget,&y,&x)==0){// check Up ,done, left ,right

        adr=arrSizeX*y+x;
        n=*(arr2+adr);

        targetListPart* tempOut;
        tempOut=arrOut.first;
        for(nOut=0;nOut<arrOut.count;nOut++){
            finishX=tempOut->x;
            finishY=tempOut->y;

            if((x==finishX)&&(y==finishY)){
                printf("!!!!!!!!!!Find Out !!!!!!!\n");
                printf("Step count=%i\n",n);
                clearTargetList(&listTarget);
                findOnePath();
                printMapCombi();
                getchar();
                return;
            }
            tempOut=tempOut->nextPart;
        }

        adr=arrSizeX*(y+1)+x;
        if(  (*(arr+adr)==' ')&&(*(arr2+adr)==0) ){
            pushTargetListPart(&listTarget,y+1,x);
            *(arr2+adr)=n+1;
        }
        adr=arrSizeX*(y-1)+x;
        if(  (*(arr+adr)==' ')&&(*(arr2+adr)==0) ){
            pushTargetListPart(&listTarget,y-1,x);
            *(arr2+adr)=n+1;
        }
        adr=arrSizeX*y+x-1;
        if(  (*(arr+adr)==' ')&&(*(arr2+adr)==0) ){
            pushTargetListPart(&listTarget,y,x-1);
            *(arr2+adr)=n+1;
        }
        adr=arrSizeX*y+x+1;
        if(  (*(arr+adr)==' ')&&(*(arr2+adr)==0) ){
            pushTargetListPart(&listTarget,y,x+1);
            *(arr2+adr)=n+1;
        }
    }
    clearTargetList(&arrOut);
    printf("!!!Exit NOT find!!!\n");
    getchar();
}

int main()
{
    core();
    return 0;
}
//====================================================
